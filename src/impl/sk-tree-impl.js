

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';
import { SkRenderEvent } from "../../../sk-core/src/event/sk-render-event.js";

const COLLAPSED_CN = 'sk-tree-collapsed';
const EXPANDED_CN = 'sk-tree-expanded';

export class SkTreeImpl extends SkComponentImpl {

    get suffix() {
        return 'tree';
    }

    get itemTplPath() {
        if (! this._itemTplPath) {
            this._itemTplPath = this.comp.itemTplPath
                || (this.comp.configEl && this.comp.configEl.hasAttribute('item-tpl-path'))
                    ? `/${this.comp.configEl.getAttribute('item-tpl-path')}/${this.prefix}-sk-${this.suffix}-item.tpl.html`
                    :`/node_modules/sk-${this.comp.cnSuffix}-${this.prefix}/src/${this.prefix}-sk-${this.suffix}-item.tpl.html`;
        }
        return this._itemTplPath;
    }

    beforeRendered() {
        super.beforeRendered();
        this.saveState();
    }

    get slot() {
        return this.comp.el.querySelector('slot');
    }

    get rootTagName() {
        if (this.comp.hasAttribute('root-tn')) {
            return this.comp.getAttribute('root-tn');
        } else {
            return 'ul';
        }
    }

    get rootClassName() {
        if (this.comp.hasAttribute('root-cn')) {
            return this.comp.getAttribute('root-cn');
        } else {
            return 'sk-tree-root';
        }
    }

    get itemTagName() {
        if (this.comp.hasAttribute('item-tn')) {
            return this.comp.getAttribute('item-tn');
        } else {
            return 'li';
        }
    }

    get itemClassName() {
        if (this.comp.hasAttribute('item-cn')) {
            return this.comp.getAttribute('item-cn');
        } else {
            return 'sk-tree-item';
        }
    }

    get branchClassName() {
        if (this.comp.hasAttribute('branch-cn')) {
            return this.comp.getAttribute('branch-cn');
        } else {
            return 'sk-tree-has-child';
        }
    }

    get containerEl() {
        if (! this._containerEl) {
            this._containerEl = this.comp.el.querySelector('.sk-tree');
        }
        return this._containerEl;
    }

    set containerEl(el) {
        this._containerEl = el;
    }

    get subEls() {
        return [ 'containerEl' ];
    }

    restoreState(state) {
        this.slot ? this.slot.innerHTML = '' : this.containerEl.innerHTML = '';
        //this.slot.insertAdjacentHTML('beforeend', state.contentsState);
    }

    bindEvents() {
        super.bindEvents();
        if (! this.onClickedHandler) {
            this.onClickedHandler = this.onClick.bind(this);
            this.comp.el.addEventListener('click', this.onClickedHandler);
        }
    }

    unbindEvents() {
        super.unbindEvents();
        if (this.onClickedHandler) {
            this.removeEventListener('click', this.onClickedHandler);
        }
    }

    afterRendered() {
        super.afterRendered();
        this.unbindEvents();
        this.clearAllElCache();
        this.rendered = false;
        this.renderImpl();
        //this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
    }

    onClick(event) {
        let el = event.target;
        if (el && el.tagName === 'LI') {
            if (el.classList.contains(COLLAPSED_CN)) {
                el.classList.remove(COLLAPSED_CN);
                el.classList.add(EXPANDED_CN);
            } else {
                if (this.comp._byParentId[el.dataset.id]) {
                    el.classList.add(COLLAPSED_CN);
                    el.classList.remove(EXPANDED_CN);
                }
            }
        }
    }

    doRender() {
        let id = this.getOrGenId();
        this.renderWithVars(id);
        let rootUl = this.comp.renderer.createEl(this.rootTagName);
        rootUl.classList.add(this.rootClassName);
        for (let item of this.comp.rootItems) {
            let itemEl = this.renderItem(item);
            rootUl.appendChild(itemEl);
        }
        if (this.selectedEl) {
            this.expandSubtree(this.selectedEl.parentElement);
        }
        if (this.slot) {
            this.slot.appendChild(rootUl);
        } else {
            if (this.containerEl) {
                this.containerEl.appendChild(rootUl);
            }
        }
        this.bindEvents();
        this.comp.bindAutoRender();
        this.comp.setupConnections();
        this.comp.callPluginHook('onRenderEnd');
        this.comp.implRenderTimest = Date.now();
        this.rendered = true;
        this.comp.removeFromRendering();
        this.comp.dispatchEvent(new CustomEvent('rendered', { bubbles: true, composed: true })); // :DEPRECATED
        this.comp.dispatchEvent(new SkRenderEvent({ bubbles: true, composed: true }));
        if (this.comp.renderDeferred) {
            this.comp.renderDeferred.resolve(this.comp);
        }
    }

    renderImpl() {
        this.comp.treeData; // load and index data if not
/*        if (this.rendered) {
            return false;
        }*/
        let themeLoaded = this.initTheme();
        themeLoaded.finally(() => { // we just need to try loading styles
            this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template
            if (!this.comp.tpl) {
                const loadTpl = async () => {
                    this.comp.tpl = await this.comp.renderer.mountTemplate(this.tplPath, this.cachedTplId,
                        this.comp, {
                            themePath: this.themePath
                        });
                };
                loadTpl().then(() => {
                    this.doRender();
                });
            } else {
                this.doRender();
            }
        });
    }

    expandSubtree(el) {
        if (el.tagName === this.itemTagName.toUpperCase()) {
            if (el.classList.contains(COLLAPSED_CN)) {
                el.classList.remove(COLLAPSED_CN);
                el.classList.add(EXPANDED_CN);
            }
        }
        if (el.tagName !== this.tagName && el.parentElement) {
            this.expandSubtree(el.parentElement);
        }

    }

    toggleSubtree(el) {
        if (el.tagName === this.itemTagName.toUpperCase() && el.classList.contains('sk-tree-has-child')) {
            if (el.classList.contains(EXPANDED_CN)) {
                el.classList.remove(EXPANDED_CN);
                el.classList.add(COLLAPSED_CN);
            } else if (el.classList.contains(COLLAPSED_CN) && el.classList.contains('sk-tree-has-child')) {
                el.classList.add(EXPANDED_CN);
                el.classList.remove(COLLAPSED_CN);
            }
        }
        if (el.tagName !== this.tagName && el.parentElement) {
            this.toggleSubtree(el.parentElement);
        }

    }

    deSelectItem() {
        if (this.comp.selectedItemEl) {
            this.comp.selectedItemEl.classList.remove('sk-selected');
            this.comp.selectedItemEl = null;
        }
    }

    selectItem(itemEl) {
        this.comp.selectedItemEl = itemEl;
        let value = this.comp.selectedItemEl.value || this.comp.selectedItemEl.dataset.value;
        if (value) {
            this.comp.setAttribute('value', value);
        }
        if (! itemEl.classList.contains('sk-selected')) {
            itemEl.classList.add('sk-selected');
        } else {
            itemEl.classList.remove('sk-selected');
        }
    }

    bindItemSelect(itemEl, item) {
        if (itemEl.clickHandler) {
            itemEl.removeEventListener('click', itemEl.clickHandler);
        }
        itemEl.clickHandler = function(event) {
            if (event.target.tagName === 'LI' && this.comp._byParentId[item.id]) {
                this.toggleSubtree(event.target || event.srcElement);
            } else {
                event.preventDefault();
                event.stopPropagation();
                this.deSelectItem();
                this.selectItem(itemEl);
                this.comp.dispatchEvent(new CustomEvent('skitemselected', {
                    detail: {value: this.comp.selectedItemEl.value, itemEl: this.comp.selectedItemEl, item: item},
                    bubbles: true,
                    composed: true
                }));
            }
        }.bind(this);
        itemEl.addEventListener('click', itemEl.clickHandler);
    }

    renderItemWithTplVars(itemEl, item) {
        let el = this.comp.renderer.prepareTemplate(this.comp.itemTpl);
        itemEl.innerHTML= this.comp.renderer.renderMustacheVars(el, {
            themePath: this.themePath,
            name: item.title || item.name,
            ...item
        });
        if (this.comp.hasAttribute('selectable')) {
            this.bindItemSelect(itemEl, item);
        }
        this.renderSubItems(itemEl, item);
    }

    renderItem(item) {
        let itemEl = this.comp.renderer.createEl(this.itemTagName);
        itemEl.classList.add(this.itemClassName);
        itemEl.setAttribute('data-id', item.id);
        itemEl.setAttribute('data-parentid', item.parentId);
        for (let itemProp of Object.keys(item)) {
            if (itemProp !== 'id' && itemProp !== 'parentId') {
                itemEl.setAttribute('data-' + this.comp.camelCase2Css(itemProp), item[itemProp]);
            }
        }
        if (this.comp.getAttribute('expanded') === 'false') {
            if (this.comp._byParentId[item.id]) {
                itemEl.classList.add(COLLAPSED_CN);
            } else {
                itemEl.classList.add(EXPANDED_CN);
            }
        } else if (this.comp.getAttribute('expanded') === 'true') {
            itemEl.classList.add(EXPANDED_CN);
        }
        if (this.comp.hasAttribute('link-tpl-str')) {
            let linkTplStr = this.comp.getAttribute('link-tpl-str');
            let itemContentsEl = this.comp.renderer.createEl('span');
            itemContentsEl.insertAdjacentHTML('beforeend', linkTplStr);
            itemEl.innerHTML = this.comp.renderer.renderMustacheVars(itemContentsEl, item);
            this.renderSubItems(itemEl, item);
        } else {
            if (this.itemTplPath) {
                if (! this.comp.itemTpl) {
                    const loadTpl = async () => {
                        this.comp.itemTpl = await this.comp.renderer.mountTemplate(this.itemTplPath, this.comp.constructor.name + 'ItemTpl',
                            this.comp, {
                                themePath: `${this.comp.basePath}/theme/${this.comp.theme}`
                        });
                    };
                    loadTpl().then(() => {
                        this.renderItemWithTplVars(itemEl, item);
                    });
                } else {
                    this.renderItemWithTplVars(itemEl, item);
                }
            } else {
                itemEl.innerHTML = item.title || item.name;
                this.renderSubItems(itemEl, item);
            }
        }
        return itemEl;
    }

    renderSubItems(itemEl, item) {
        // render subitems
        if (this.comp._byParentId[item.id]) {
            let itemSubEl = this.comp.renderer.createEl(this.rootTagName);
            for (let subItem of this.comp._byParentId[item.id]) {
                let subItemEl = this.renderItem(subItem);
                if (this.comp.hasAttribute('selectable')) {
                    this.bindItemSelect(subItemEl, subItem);
                }
                itemSubEl.appendChild(subItemEl);
            }
            itemEl.appendChild(itemSubEl);
            itemEl.classList.add(this.branchClassName);
        }
        let selectedId = this.comp.getAttribute('selected-id');
        if (selectedId !== null && selectedId === item.id) {
            this.selectedEl = itemEl;
        }
    }
}