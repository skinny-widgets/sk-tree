
import { SkElement } from '../../sk-core/src/sk-element.js';


export class SkTree extends SkElement {

    get cnSuffix() {
        return 'tree';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    buildIdCache() {
        this._byId = {};
        this._byParentId = {};
        for (let category of this._treeData) {
            this._byId[category.id] = category;
            if (! this._byParentId[category.parentId]) {
                this._byParentId[category.parentId] = [];
            }
            this._byParentId[category.parentId].push(category);
        }
    }

    get treeData() {
        if (! this._treeData) {
            let value = this.getAttribute('tree-data');
            this._treeData = value ? JSON.parse(value) : [];
            this.buildIdCache();
        }
        return this._treeData;
    }

    set treeData(treeData) {
        this._treeData = treeData;
        this.setAttribute('tree-data', JSON.stringify(treeData));
        this.buildIdCache();
    }

    get rootItems() {
        return this._byParentId[0] || [];
    }

    render() {
        if (this.implRenderTimest) {
            this.impl.rendered = false;
            this.impl.clearAllElCache();
        }
        super.render();
    }
}
